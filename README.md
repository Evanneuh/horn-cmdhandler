# horn-cmdhandler
> An NPM Package to make creating new Discord.JS bots efficiently

[![NPM Version][npm-image]][npm-url]
[![Discord Stats][discord-image]][discord-url]

Horn-cmdhandler currently includes a command handler for your Discord.JS bots.


## Installation

```sh
npm install horn-cmdhandler --save
```

## Setup guide
1 - To start using the Command Handler after installation, we'll first need require `horn-cmdhandler` and create a new Command Handler with the proper folder name and prefixes.

```js
const { CommandHandler } = require('horn-cmdhandler')
const CH = new CommandHandler({
    folder: __dirname + '/commands/',
    prefix: ['?', '+', 'a!']
  });
```

2 - Inside of the message event, we're going to do a little parsing and checking if they ran an available command or not.

```js
bot.on("message", (message) => {
    if(message.channel.type === 'dm') return
    if(message.author.type === 'bot') return
    let args = message.content.split(" ")
    let command = args[0]
    let cmd = CH.getCommand(command)
    if (!cmd) return

    try {
        cmd.run(bot, message, args)
    }catch(e) {
        console.log(e)
    }

});
```

3 - And of course we're going to need a command file. So inside of your bot folder, create a folder called commands. I'm going to create a file called
test.js and put the following code inside of it.

```js
module.exports = class test {
    constructor(){
            this.name = 'test',
            this.alias = ['t'],
            this.usage = '?test'
    }

    async run(bot, message, args) {
        await message.delete()
        message.reply(`${this.name} worked!`)
    }
}
```

4 - And that's it! You have a working command handler now for all the commands you could want!

[https://gitlab.com/Evanneuh/horn-cmdhandler](https://gitlab.com/Evanneuh/horn-cmdhandler)

## Contributing

1. Fork it (<https://gitlab.com/Evanneuh/horn-cmdhandler/fork>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request

<!-- Markdown link & img dfn's -->
[npm-image]: https://img.shields.io/npm/v/horn-cmdhandler.svg?style=for-the-badge&logo=npm
[npm-url]: https://www.npmjs.com/package/horn-cmdhandler
[discord-url]: https://discord.gg/swSw5D
[discord-image]: https://img.shields.io/discord/468050613431959558.svg?style=for-the-badge&logo=discord